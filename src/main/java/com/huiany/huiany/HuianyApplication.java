package com.huiany.huiany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class HuianyApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuianyApplication.class, args);
	}

}
