package com.huiany.huiany.customer;

import java.util.List;

public interface CustomerRepo {
    List<Customer> getCustomers();
}
