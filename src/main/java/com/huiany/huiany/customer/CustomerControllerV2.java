package com.huiany.huiany.customer;

import com.huiany.huiany.exception.ApiRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@RequestMapping(path="api/v2/customers")
@RestController
public class CustomerControllerV2 {

    private final CustomerService customerService;

    @Autowired
    public CustomerControllerV2(CustomerService customerService) {

        this.customerService = customerService;
    }

    @GetMapping
    List<Customer> getCustomers() {

        //return customerService.getCustomers();
        return customerService.getCustomers();
        //return Collections.singletonList(
                //new Customer(0,"v2","123")
        //);
    }

    @GetMapping(path="{customerId}")
    Customer getCustomer(@PathVariable("customerId") Long id){
        //return new Customer(0,"v2","123");
        /*return customerService.getCustomers()
                .stream()
                .filter(customer -> customer.getId()==id)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("customer 无法找到"));*/
        return customerService.getCustomer(id);
    }

    @GetMapping(path = "{customerId}/exception")
    Customer getCustomerException(@PathVariable("customerId") int id){
        throw new ApiRequestException(
                "ApiRequestException for customer" + id
        );
    }

    @PostMapping
    void createNewCustomer(@Valid @RequestBody Customer customer){
        System.out.println("POST 请求...");
        System.out.println(customer);
    }

    @PutMapping
    void updateCustomer(@RequestBody Customer customer){
        System.out.println("UPDATE 请求");
        System.out.println(customer);
    }

    @DeleteMapping(path = "{customerId}")
    void deleteCustomer(@PathVariable("customerId") int id){
        System.out.println("DELETE 请求,删除 Customer,其ID为"+id);
    }
}
