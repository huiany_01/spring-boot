package com.huiany.huiany.customer;

import java.util.Arrays;
import java.util.List;


public class CustomerFakeRepository implements CustomerRepo{

    @Override
    public List<Customer> getCustomers(){
    return Arrays.asList(
            new Customer(233l,"hgdsh","gryge", "email@gemail.com"),
            new Customer(344l,"weeeeef","uefufe", "email@gemail.com")
            );
    }
}
