package com.huiany.huiany.customer;

import com.huiany.huiany.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CustomerService {

    private final static Logger LOGGER =  LoggerFactory.getLogger(CustomerService.class);
    private final CustomerRepository customerRepository;

    @Autowired
    public  CustomerService(CustomerRepository customerRepository){
        this.customerRepository=customerRepository;
    }

   List<Customer> getCustomers(){
        LOGGER.info("getCustomers called");
        return customerRepository.findAll();
   }

   Customer getCustomer(Long id){
       return customerRepository
               .findById(id)
               .orElseThrow(() -> {
                   NotFoundException notFoundException=new NotFoundException("customer " + id + " 无法找到");
                   LOGGER.error("error:getCustomer {} " + id, notFoundException);
                   return notFoundException;
               });
   }
}